Categories:Games
License:GPLv3
Web Site:https://github.com/ligi/gobandroid/blob/HEAD/README.md
Source Code:https://github.com/ligi/gobandroid
Issue Tracker:https://github.com/ligi/gobandroid/issues
Changelog:https://github.com/ligi/gobandroid/commits/HEAD

Auto Name:Gobandroid
Summary:Ancient Go game
Description:
Gobandroid is virtual goban (GO-Board) on your mobile to study and play the
ancient game of Go (weiqi in Chinese, igo in Japanese, baduk in Korean).

Go originated in China more than 2,000 years ago. The game is noted for being
rich in strategy despite its relatively simple rules. It has been claimed that
Go is the most complex game in the world due to its vast number of variations in
individual games. More info on [http://en.wikipedia.org/wiki/Go_(game)
Wikipedia].

Features:

* Solve Tsumegos (Go Problems are called Tsumego)
* Review Games (SGF Format reader)
* Record Games (or use the phone/tablet as a board - incl SGF writer)
* TV-Mode (automatic replay of games - mainly for GoogleTV)
* Play against GnuGo (extra install needed)
.

Repo Type:git
Repo:https://github.com/ligi/gobandroid.git

Build:1.28,128
    commit=1.28
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:1.29,129
    commit=1.29
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:1.9.0,190
    commit=1.9.0
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:1.9.2,192
    commit=1.9.2
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:1.9.3,193
    commit=1.9.3
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:1.9.4,194
    commit=1.9.4
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.0,200
    commit=2.0.0
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.1,201
    commit=2.0.1
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.3,203
    commit=2.0.3
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.4,204
    commit=2.0.4
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.5,205
    commit=2.0.5
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.6,206
    commit=2.0.6
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.7,207
    commit=2.0.7
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.8,208
    commit=2.0.8
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.0.9,209
    commit=2.0.9
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.1.0,210
    disable=wrong tag
    commit=2.1.0
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.1.1,211
    commit=2.1.1
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.1.2,212
    commit=2.1.2
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.1.3,213
    commit=2.1.3
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Build:2.1.4,214
    commit=2.1.4
    subdir=android
    gradle=NoAnalyticsNoCloud
    prebuild=sed -i -e '/play_services/d' -e '/play-services/d' -e '/android-sdk-manager/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.1.4
Current Version Code:214
