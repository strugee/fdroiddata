Categories:Reading
License:GPLv3
Web Site:https://github.com/ligi/PassAndroid/blob/HEAD/README.md
Source Code:https://github.com/ligi/PassAndroid
Issue Tracker:https://github.com/ligi/PassAndroid/issues

Auto Name:PassAndroid
Summary:View Passbook files
Description:
Displays Passbook (*.pkpass) files & shows the Barcode (QR, PDF417 and AZTEC
format). It can be used also when offline.
.

Repo Type:git
Repo:https://github.com/ligi/PassAndroid

Build:2.3.1,231
    commit=2.3.1
    gradle=NoMapsNoAnalyticsForFDroid
    prebuild=sed -i '/play_services/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Build:2.3.2,232
    commit=2.3.2
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.3.5,235
    commit=2.3.5
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.3.6,236
    commit=2.3.6
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.3.7,237
    commit=2.3.7
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.3.9,239
    commit=2.3.9
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.4.1,241
    commit=2.4.1
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle && \
        sed -i -e '/classpath/a*\/' build.gradle && \
        sed -i -e '/classpath/iclasspath "com.android.tools.build:gradle:0.12.+"\n\/*\n' build.gradle

Build:2.4.4,244
    commit=2.4.4
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle && \
        sed -i -e '/buildTypes/i/*' -e '/compileOptions/i*/' build.gradle

Build:2.4.5,245
    commit=2.4.5
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/com.stanfy.spoon:spoon-gradle-plugin/,+3d' build.gradle && \
        sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle && \
        sed -i -e '/buildTypes/i/*' -e '/compileOptions/i*/' build.gradle

Build:2.4.7,247
    commit=2.4.7
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/com.stanfy.spoon:spoon-gradle-plugin/,+3d' build.gradle && \
        sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle && \
        sed -i -e '/buildTypes/i/*' -e '/compileOptions/i*/' build.gradle

Build:2.4.8,248
    commit=2.4.8
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/com.stanfy.spoon:spoon-gradle-plugin/,+3d' build.gradle && \
        sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle && \
        sed -i -e '/buildTypes/i/*' -e '/compileOptions/i*/' build.gradle

Build:2.4.9,249
    commit=2.4.9
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/com.stanfy.spoon:spoon-gradle-plugin/,+2d' build.gradle && \
        sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.5.0,250
    commit=2.5.0
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/com.stanfy.spoon:spoon-gradle-plugin/,+2d' build.gradle && \
        sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.5.3,253
    commit=2.5.3
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play_services/d' -e '/android-sdk-manager/d' -e '/spoon/d' build.gradle

Build:2.5.4,254
    commit=2.5.4
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' build.gradle && \
        sed -i -e '/spoon/d' build.gradle

Build:2.5.5,255
    commit=2.5.5
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' build.gradle && \
        sed -i -e '/spoon/d' build.gradle

Build:2.5.8,258
    commit=2.5.8
    subdir=android
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' -e '/spoon {/,+2d' -e '/spoon/d' build.gradle

Build:2.6.0,260
    commit=2.6.0
    subdir=android
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' -e '/spoon {/,+2d' -e '/spoon/d' build.gradle

Build:2.6.2,262
    commit=2.6.2
    subdir=android
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' -e '/spoon {/,+2d' -e '/spoon/d' build.gradle

Build:2.6.3,263
    commit=2.6.3
    subdir=android
    gradle=noMaps,noAnalytics,forFDroid
    prebuild=sed -i -e '/play-services/d' -e '/android-sdk-manager/d' -e '/spoon-gradle-plugin/,+2d' -e '/spoon {/,+2d' -e '/spoon/d' build.gradle

Maintainer Notes:
TODO: Build zxing-core-2.3.0-SNAPSHOT.jar from source.
* https://github.com/zxing/zxing
* cd core && mvn -DskipTests package
* cp core/target/core-x.y.z.jar
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.6.3
Current Version Code:263
