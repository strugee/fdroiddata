Categories:Connectivity
License:GPLv3
Web Site:https://forums.getpebble.com/discussion/5767/android-pebbledialer-new-call-controls-for-your-pebble
Source Code:https://github.com/matejdro/PebbleDialer-Android
Issue Tracker:https://github.com/matejdro/PebbleDialer-Android/issues

Auto Name:Dialer for Pebble
Summary:Client for the PebbleDialer watchapp
Description:
Pebble dialer gives you extra options for phone calls on your pebble, such as:

* Answer incoming calls
* Outgoing calls from the pebble
* Mute ringer of incoming calls
* Toggle in-call microphone mute
* Toggle speakerphone

This is intended to use with the official closed source Pebble App, but
[[nodomain.freeyourgadget.gadgetbridge]] from f-droid also works at least
partially.

The needed watchapp for the pebble can be downloaded
[https://dl.dropboxusercontent.com/u/6999250/dialer/PebbleDialer.pbw here].

Crashlytics support and internet permission have been removed.
.

Repo Type:git
Repo:https://github.com/matejdro/PebbleDialer-Android.git

Build:2.4,24
    commit=97cfcaeb13f47b341795afea4b4c31134057c7d1
    subdir=app
    patch=remove_crashlytics_and_internet_permission.diff
    gradle=yes
    srclibs=AndroidPebbleCommons@9e3017a233a1a1e7d06354e064068c30dcf5288f
    prebuild=sed -i 's_../PebbleAndroidCommons_app/$$AndroidPebbleCommons$$_' ../settings.gradle

Build:2.41,25
    commit=2.41
    subdir=app
    patch=remove_crashlytics_and_internet_permission_2.41.diff
    gradle=yes
    srclibs=AndroidPebbleCommons@r1
    prebuild=sed -i 's_../PebbleAndroidCommons_app/$$AndroidPebbleCommons$$_' ../settings.gradle

Build:2.42,26
    commit=2.42
    subdir=app
    patch=remove_crashlytics_and_internet_permission_2.41.diff
    gradle=yes
    srclibs=AndroidPebbleCommons@r1
    prebuild=sed -i 's_../PebbleAndroidCommons_app/$$AndroidPebbleCommons$$_' ../settings.gradle

Build:2.52,28
    commit=2.52
    subdir=app
    patch=remove_crashlytics_and_internet_permission_2.52.diff
    gradle=yes
    srclibs=AndroidPebbleCommons@r5
    prebuild=sed -i 's_../PebbleAndroidCommons_app/$$AndroidPebbleCommons$$_' ../settings.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.55
Current Version Code:31
